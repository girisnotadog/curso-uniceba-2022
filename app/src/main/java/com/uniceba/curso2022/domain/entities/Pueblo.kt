package com.uniceba.curso2022.domain.entities

class Pueblo(override val nombre: String): CentroUrbano {
    override var habitantes: List<Persona> = listOf()
    override fun elegirLider(lider: String) {
        TODO("Not yet implemented")
    }

    val atraccionPrincipal: String = "Feria del pueblo"
}