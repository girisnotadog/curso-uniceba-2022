package com.uniceba.curso2022.domain.entities

data class Perro(
    val nombre: String,
    val raza: String,
    var edad: Int,
)
