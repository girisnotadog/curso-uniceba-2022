package com.uniceba.curso2022.domain.entities

// Contrato
interface CentroUrbano {
    val nombre: String
    val habitantes: List<Persona>

    fun elegirLider( lider: String )
}