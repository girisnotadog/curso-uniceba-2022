package com.uniceba.curso2022.domain.entities

import android.util.Log

class Persona( val nombre: String ) {
    fun saludarMucho() {
        var contador: Int = 2015

        while ( contador <= 2025 ) {
            saludar( contador )
            // contador = contador + 1
            contador ++
        }
    }

    fun saludar( year: Int ) {
        var saludo: String = ""

        if ( year > 2022 )
            saludo = "Hola Uniceba, es el año $year"
        else
            saludo = "Me llamo $nombre, es el año $year"

        Log.d("UNICEBA_app", saludo)
    }
}