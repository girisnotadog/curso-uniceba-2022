package com.uniceba.curso2022.presentation.fragments

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.uniceba.curso2022.R
import com.uniceba.curso2022.databinding.FragmentSharedPreferencesBinding

class SharedPreferencesFragment : Fragment() {
    private lateinit var binding: FragmentSharedPreferencesBinding
    private lateinit var sharedPrefs: SharedPreferences

    val nombreLlaveSharedPreferences = "miPrimeraLlave"

    /**
     * Podemos guardar en sharedPreferences:
     * El nombre de usuario, correo
     * Avance de un juego
     * Datos del cliente
     * Productos o carrito*
     * Estado de una compra (terminada / No terminada)
     */

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSharedPreferencesBinding.inflate( inflater, container, false )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Cuando la vista sea creada, asignamos el valor a la variable de preferencias locales
        // Requiere el contexto de la actividad
        sharedPrefs = activity?.getSharedPreferences(
            "shared_preferences_uniceba", Context.MODE_PRIVATE
        ) !!

        sharedPrefs?.registerOnSharedPreferenceChangeListener { sharedPreferences, key ->
            Log.d("SharedPreferences","Cambio registrado en $key")
        }

        binding.fragmentSharedPreferencesLoad.setOnClickListener {
            // Obtenemos la llave de las preferencias locales y definimos un valor default
            val valorAlmacenado = sharedPrefs?.getString(nombreLlaveSharedPreferences, "No tengo información")

            Log.d("ValorAlmacenado", "$valorAlmacenado")
            // Definios el contenido del texto almacenado en el cuadro de texto
            binding.fragmentSharedPreferencesStoredText.setText( valorAlmacenado )

            Toast.makeText( context, "Valor cargado!", Toast.LENGTH_SHORT).show()
        }

        binding.fragmentSharedPreferencesSave.setOnClickListener {

            // Obtenemos el valor del cuadro de texto
            val valorAGuardar = binding.fragmentSharedPreferencesStoredText.text.toString()

            sharedPrefs?.
                // Ponemos las preferencias en modo edición
            edit()?.
                // Ponemos un String en las preferencias con la llave indicada
            putString(nombreLlaveSharedPreferences, valorAGuardar)?.
                // Aplicamos los cambios a las preferencias locales
            apply()

            Log.d("valorAGuardar", "$valorAGuardar")
            Toast.makeText( context, "Valor guardado!", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        fun newInstance() = SharedPreferencesFragment()
    }
}