package com.uniceba.curso2022.presentation.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.uniceba.curso2022.R
import com.uniceba.curso2022.data.dto.AlumnosDto

class RecyclerViewAdapter(
    val listaElementos: List<AlumnosDto>,
    val llamadaClickElemento: (String) -> Unit
): RecyclerView.Adapter<RecyclerViewAdapter.MiViewHolder>(){

    // Clase que infla el contenido de la lista
    inner class MiViewHolder(val miVista: View): RecyclerView.ViewHolder(miVista) {
        val titulo: TextView = miVista.findViewById(R.id.fragment_list_item_text )
        val elemento: ConstraintLayout = miVista.findViewById(R.id.fragment_list_item_layout)

        fun enlazaItemLista(alumno: AlumnosDto) {
            titulo.text = alumno.name
            // Asociamos el click del elemento en la llamada al click del constructor del adapter
            elemento.setOnClickListener {
                llamadaClickElemento( alumno.id )
            }
        }
    }

    // Creamos el contenedor para la vista de elementos
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MiViewHolder {
        return MiViewHolder(
            miVista = LayoutInflater.from( parent.context )
                .inflate(
                    R.layout.fragment_list_item, parent, false
                )
        )
    }

    // Enlazamos el elemento de la lista con los datos de la lista de elementos
    override fun onBindViewHolder(holder: MiViewHolder, position: Int) {
        holder.enlazaItemLista(
            // Obtenemos el elemendo en la posición dada
            listaElementos.get( position )
        )
    }

    // Contamos los elementos a pintar
    override fun getItemCount(): Int {
        return listaElementos.size
    }
}