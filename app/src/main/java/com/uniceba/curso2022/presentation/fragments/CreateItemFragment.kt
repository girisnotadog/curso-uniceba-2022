package com.uniceba.curso2022.presentation.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.uniceba.curso2022.R
import com.uniceba.curso2022.data.dto.AlumnosDto
import com.uniceba.curso2022.databinding.FragmentCreateItemBinding
import com.uniceba.curso2022.presentation.activities.CrudActivity
import com.uniceba.curso2022.presentation.viewmodels.ListFragmentViewModel

class CreateItemFragment : Fragment() {
    private lateinit var binding: FragmentCreateItemBinding
    private lateinit var listFragmentViewModel: ListFragmentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        listFragmentViewModel = ViewModelProvider(this).get(ListFragmentViewModel::class.java)

        binding = FragmentCreateItemBinding.inflate(
            layoutInflater,
            container,
            false
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val idEdicion = arguments?.getString("id")

        Log.d("IDEdicion", "$idEdicion")

        if( idEdicion != null) {
            listFragmentViewModel.editarRegistro( idEdicion )

            listFragmentViewModel.alumnoEdicion.observe( viewLifecycleOwner ) {
                binding.fragmentCreateItemPet.setText( it.pet )
                binding.fragmentCreateItemName.setText( it.name )
                binding.fragmentCreateItemAvatar.setText( it.avatar )
                binding.fragmentCreateItemAge.setText( it.age )

                binding.fragmentCreateItemDelete.visibility = View.VISIBLE
            }
        }

        binding.fragmentCreateItemSave.setOnClickListener {
            // Al dar clic en guardar, preparamos la información a enviar al servicio
            val nuevoAlumno = AlumnosDto(
                binding.fragmentCreateItemPet.text.toString(),
                binding.fragmentCreateItemName.text.toString(),
                binding.fragmentCreateItemAvatar.text.toString(),
                binding.fragmentCreateItemAge.text.toString(),
            )

            if( idEdicion != null ) {
                // Estamos editando un registro
                listFragmentViewModel.actualizarAlumno(
                    idEdicion,
                    nuevoAlumno
                )

                Toast.makeText(context, "Registro actualizado!!!", Toast.LENGTH_SHORT).show()
            }else{
                // Es un nuevo registro
                listFragmentViewModel.guardarAlumno(
                    nuevoAlumno
                )

                Toast.makeText(context, "Registro creado!!!", Toast.LENGTH_SHORT).show()
            }

            // Indicando que la actividad es de tipo CrudActivity
            val actividadPadre = activity as CrudActivity?
            actividadPadre?.retroceder()
        }

        binding.fragmentCreateItemDelete.setOnClickListener {
            if( idEdicion != null)
                listFragmentViewModel.eliminarRegistro( idEdicion )

            val actividadPadre = activity as CrudActivity?
            actividadPadre?.retroceder()
        }
    }

    companion object {
        fun newInstance(id: String? = null): CreateItemFragment {
            val fragmentoNuevo = CreateItemFragment()

            if( id != null) {
                fragmentoNuevo.arguments = Bundle()
                fragmentoNuevo.arguments?.putString("id", id)
            }

            return fragmentoNuevo
        }
    }
}