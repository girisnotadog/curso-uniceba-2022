package com.uniceba.curso2022.presentation.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.uniceba.curso2022.R
import com.uniceba.curso2022.databinding.FragmentMascotaBinding
class MascotaFragment : Fragment() {
    private lateinit var binding: FragmentMascotaBinding
    private var nombreMascota: String = ""
    private var tipoMascota: String = ""

    private var contador: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflamos el fragmento
        binding = FragmentMascotaBinding.inflate( layoutInflater, container, false )

        // Obtenemos lo argumentos y los pasamos a unas variables
        nombreMascota = arguments?.getString("nombre") ?: "Sin nombre"
        tipoMascota = arguments?.getString("tipo") ?: "Sin tipo"

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Se va a ejecutar cuando la vista ya ha sido creada

        // Representamos el valor de las variables en la interfaz gráfica
        binding.fragmentMascotaNombre.text = nombreMascota
        binding.fragmentMascotaTipo.text = tipoMascota

        binding.fragmentMascotaBoton1.setOnClickListener {
            Toast.makeText( context, "Mascota: $nombreMascota", Toast.LENGTH_SHORT).show()
            binding.fragmentMascotaContador.text = "$contador"
            contador ++
        }
    }

    // El contenido dentro de este companion es accesible de forma estática: Sin crear una instancia
    companion object {
        fun newInstance(nombre: String, tipo: String): Fragment {
            // Creamos el fragmento nuevo
            val fragmentoNuevo: Fragment = MascotaFragment()
            // Creamos el contenedor de argumentos
            fragmentoNuevo.arguments = Bundle()
            // Asignamos el valor a los argumentos
            fragmentoNuevo.arguments?.putString("nombre", nombre)
            fragmentoNuevo.arguments?.putString("tipo", tipo)

            // Devolvemos el nuevo fragmento
            return fragmentoNuevo
        }
    }
}