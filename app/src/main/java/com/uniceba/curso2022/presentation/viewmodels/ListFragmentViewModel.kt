package com.uniceba.curso2022.presentation.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.uniceba.curso2022.data.ClienteAPI
import com.uniceba.curso2022.data.dto.AlumnosDto
import kotlinx.coroutines.launch

class ListFragmentViewModel: ViewModel() {
    // Defino una variable privada de tipo LiveData MUTABLE
    // Solo se puede acceder a ella desde dentro del viewModel (la clase)
    private val _listaAlumnos: MutableLiveData<List<AlumnosDto>> = MutableLiveData(
        listOf()
    )

    // Registro para editar
    private val _alumnoEdicion: MutableLiveData<AlumnosDto> = MutableLiveData(
        AlumnosDto()
    )

    val alumnoEdicion: LiveData<AlumnosDto> = _alumnoEdicion

    // Defino una variable de tipo LiveData NO Mutable
    // Accesible desde fuera del viewModel (la clase)
    // Asignamos su valor igual al de _listaSitiosWeb pero lo hacemo inmutable por medio
    // de un CAST
    val listaAlumnos: LiveData<List<AlumnosDto>> = _listaAlumnos

    // Defino una función que modifica el valor de mi LiveData mutable ( Lo muta )
    // Esta función está dentro de la clase, puede modificarla
    fun getListaElementos() {
        // Hacemos la llamada al servicio
        viewModelScope.launch {
            // Lanzamos una función que puede detener el hilo (ejecución) principal
            // INICIAMOS UNA CORUTINA
            try {
                // Trampa por si ocurre un error en la petición
                _listaAlumnos.value = ClienteAPI.miServicio.obtenerEntradas()
            }catch (e: Exception) {
                Log.d("ConsumoServicio", "Ocurrión un error: ${e.localizedMessage}")
            }
        }
    }

    fun guardarAlumno(alumno: AlumnosDto) {
        viewModelScope.launch {
            try {
                // Guardamos el nuevo registro
                ClienteAPI.miServicio.guardarEntrada( alumno )
                // Actualizamos la lista de elementos
                getListaElementos()
            }catch (e: Exception) {
                Log.d("ConsumoServicio", "Ocurrión un error: ${e.localizedMessage}")
            }
        }
    }

    // Guarda el registro
    fun actualizarAlumno(id: String, alumno: AlumnosDto) {
        viewModelScope.launch {
            try {
                // Guardamos el nuevo registro
                ClienteAPI.miServicio.actualizarEntrada( id, alumno )
                // Actualizamos la lista de elementos
                getListaElementos()
            }catch (e: Exception) {
                Log.d("ConsumoServicio", "Ocurrión un error: ${e.localizedMessage}")
            }
        }
    }

    // Obtiene el registro
    fun editarRegistro(id: String) {
        viewModelScope.launch {
            try {
                // Obtenemos el regisro
                val registroObtenido = ClienteAPI.miServicio.obtenerRegistro( id )
                // Editamos el registro que obtuve del servicio
                _alumnoEdicion.value = registroObtenido
            }catch (e: Exception) {
                Log.d("ConsumoServicio", "Ocurrión un error: ${e.localizedMessage}")
            }
        }
    }

    fun eliminarRegistro( id: String ) {
        viewModelScope.launch {
            try {
                // Eliminamos el registro indicado
                ClienteAPI.miServicio.eliminarRegistro( id )

                // Actualizamos la lista de registros
                getListaElementos()
            }catch (e: Exception) {
                Log.d("ConsumoServicio", "Ocurrión un error: ${e.localizedMessage}")
            }
        }
    }
}