package com.uniceba.curso2022.presentation.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.uniceba.curso2022.databinding.FragmentListBinding
import com.uniceba.curso2022.presentation.activities.CrudActivity
import com.uniceba.curso2022.presentation.adapters.RecyclerViewAdapter
import com.uniceba.curso2022.presentation.viewmodels.ListFragmentViewModel

class ListCrudFragment : Fragment() {
    private lateinit var binding: FragmentListBinding
    private lateinit var miViewModel: ListFragmentViewModel

    // Se ejecuta al crear la vista
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Enlazamos el viewModel al fragmento
        miViewModel = ViewModelProvider( this ).get(ListFragmentViewModel::class.java)
        // Inflamos el XML
        binding = FragmentListBinding.inflate( layoutInflater, container, false )

        return binding.root
    }

    // Se ejecuta una vez creada la vista
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Definimos el administrador de plantillas
        binding.fragmentListRvContent.layoutManager =
            LinearLayoutManager( context, LinearLayoutManager.VERTICAL, false)

        miViewModel.listaAlumnos.observe(viewLifecycleOwner) { apiListAlumnos ->
            // Asignamos el adaptador a la lista
            binding.fragmentListRvContent.adapter = RecyclerViewAdapter(
                listaElementos = apiListAlumnos,
                // Pasamo la función como referencia al parámetro
                //llamadaClickElemento = ::seleccionarElemento

                // Pasamos una Lambda como parámetro
                llamadaClickElemento = {
                    seleccionarElemento( it )
                }
            )

            binding.fragmentListSwipeRefresh.isRefreshing = false
        }

        miViewModel.getListaElementos()

        // Agregamos el escuchador al swipe to refresh
        binding.fragmentListSwipeRefresh.setOnRefreshListener {
            miViewModel.getListaElementos()
        }
    }

    fun seleccionarElemento(cualElemento: String) {
        val actividadPadre = activity as CrudActivity?
        actividadPadre?.editar( cualElemento )
    }

    companion object {
        fun newInstance() = ListCrudFragment()
    }
}