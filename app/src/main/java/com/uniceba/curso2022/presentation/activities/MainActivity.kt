package com.uniceba.curso2022.presentation.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.uniceba.curso2022.databinding.ActivityMainBinding
import com.uniceba.curso2022.presentation.viewmodels.MainActivityViewModel
import java.util.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    // Inicialización tardía => Promesa de inicialización
    private lateinit var enlaceALaVista: ActivityMainBinding
    private lateinit var mainActivityViewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Asigna el viewModel del activity a partir del proveedor de viewModels
        mainActivityViewModel = ViewModelProvider(this)
                // Devuelve la instancia de MainActivityViewModel
            .get( MainActivityViewModel::class.java )

        enlaceALaVista = ActivityMainBinding.inflate(layoutInflater)
        val view = enlaceALaVista.root
        setContentView(view)

        inicializar()
    }

    fun inicializar() {
        // setContentView(R.layout.activity_main)
        //val botonClicAqui = findViewById<AppCompatButton>(R.id.button_click_me)
        val botonClicAqui = enlaceALaVista.buttonClickMe

        botonClicAqui.setOnClickListener {
            pruebaIntentExplicito()
            // pruebaIntentImplicito()
        }

        // MainActivity es el PROPIETARIO del observador
        mainActivityViewModel.cajaTexto.observe(this) {
            // Recibimos la llamada de la función lambda con el parámetro "it"
            enlaceALaVista.activityMainEntradaTexto.setText( it )
        }

        enlaceALaVista.activityMainBotonEditarTexto.setOnClickListener {
            // Llamamos a la función del viewModel para definir un valor
            mainActivityViewModel.setCajaTexto(
                // Generamos un texto aleatorio
                UUID.randomUUID().toString()
            )
        }

        enlaceALaVista.barraProgreso.min = 0
        enlaceALaVista.barraProgreso.max = 100

        enlaceALaVista.botonValorAleatorio.setOnClickListener {
            val nuevoValor: Int = Random.nextInt(1, 100)

            enlaceALaVista.barraProgreso.progress = nuevoValor

            Toast.makeText(applicationContext, "Cambio a $nuevoValor", Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * Dejamos que el sistema determine como satisfacer nuestra intención
     */
    fun pruebaIntentImplicito() {
        // Creamos una intención
        val intencion = Intent()

        // Definimos la accion de "Ver" a la intención
        intencion.action = Intent.ACTION_MAIN
        // Queremos que la intención se ejecute en una nueva tarea
        intencion.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        // Define la categoria como Aplicación de mensajes
        intencion.addCategory( Intent.CATEGORY_APP_MESSAGING )

        // Iniciamos la nueva actividad asociada a la intención
        startActivity( intencion )
    }

    /**
     * Definimos el objetivo que quermos abrir con nuestra intención
     */
    fun pruebaIntentExplicito() {
        // Creamos una intención
        val intencion = Intent()

        // Definimos la accion de "Ver" a la intención
        intencion.action = Intent.ACTION_VIEW
        // Queremos que la intención se ejecute en una nueva tarea
        intencion.flags = Intent.FLAG_ACTIVITY_NEW_TASK

        val nuevoValor: Int = Random.nextInt(1, 100)

        // Mandamos un extra a la intención
        intencion.putExtra("saludo", "Hola desde Main activity: $nuevoValor")

        // Definimos el objetivo (Clase) a la intención
        intencion.setClass( applicationContext, PersonActivity::class.java )

        // Iniciamos la nueva actividad asociada a la intención
        startActivity( intencion )
    }
}

// MVvM = Model View ViewModel -> Android Kotlin
// MVC = Model View Controller -> Swift / Backend
// MVP = Model View Presenter -> Android JAVA

/*
 * Ejemplos:
 *        val alguien = Persona("Jacobo")
alguien.saludar( 2043)

val fulanita = Persona("Maria")
fulanita.saludarMucho()

val menganito = Persona("Juan")
menganito.saludarMucho()

val celaya = Ciudad("Celaya")
val puebloFantasma = Pueblo("Mineral de pozos")
val tamayo = Comunidad("Comunidad de tamayo")

celaya.bomberos.add( alguien )
celaya.bomberos.add( fulanita )

celaya.bomberos.forEach {
Log.d("Bombero", it.nombre)
}
*
* Diferencia Vacío vs Nulo

    var nombreVacio: String = ""
    var nombreNulo: String? = null

    Log.d("NOMBRE", nombreVacio)

    if( nombreNulo != null )
        Log.d("NOMBRE Nulo", nombreNulo)
*/