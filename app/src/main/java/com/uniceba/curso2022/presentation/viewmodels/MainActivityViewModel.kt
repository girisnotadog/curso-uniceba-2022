package com.uniceba.curso2022.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

// Defino el viewModel extendiendo de ViewModel()
class MainActivityViewModel: ViewModel() {
    // Defino una variable privada de tipo LiveData MUTABLE
    // Solo se puede acceder a ella desde dentro del viewModel (la clase)
    private val _cajaTexto: MutableLiveData<String> = MutableLiveData("")

    // Defino una variable de tipo LiveData NO Mutable
    // Accesible desde fuera del viewModel (la clase)
    // Asignamos su valor igual al de _cajaTexto pero lo hacemo inmutable por medio
    // de un CAST
    val cajaTexto: LiveData<String> = _cajaTexto

    // Defino una función que modifica el valor de mi LiveData mutable ( Lo muta )
    // Esta función está dentro de la clase, puede modificarla
    fun setCajaTexto(nuevoValor: String) {
        _cajaTexto.value = nuevoValor
    }
}