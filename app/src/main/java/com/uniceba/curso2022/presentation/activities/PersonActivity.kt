package com.uniceba.curso2022.presentation.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.uniceba.curso2022.databinding.ActivityPersonBinding
import com.uniceba.curso2022.presentation.fragments.SharedPreferencesFragment

class PersonActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPersonBinding
    private var contador: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityPersonBinding.inflate( layoutInflater )
        setContentView( binding.root )

        if( intent.hasExtra("saludo") )
            binding.activityPersonSaludo.text = intent.getStringExtra("saludo")

        inicializar()
    }

    fun inicializar() {
        binding.activityPersonAgregarFragmento.setOnClickListener {
            // Obtuve una instancia del fragmento mascota
//            val fragmento: Fragment = MascotaFragment.newInstance(
//                "Fido $contador",
//                "Perro"
//            )

            // val fragmento = ListFragment.newInstance()
            val fragmento = SharedPreferencesFragment.newInstance()

            // Inicamos una transacción para el administrador de fragmentos
            supportFragmentManager.beginTransaction()
                    // Reemplazamos el contenido del administrador de fragmentos
                .replace( binding.fragmentContainerView.id, fragmento )
                // Agregamos el nombre a la pila de fragmentos
                .addToBackStack("")
                // Enviamos la transacción
                .commit()

            // Incrementamos el contador
            contador ++
        }

        binding.activityPersonQuitarFragmento.setOnClickListener {
            if( supportFragmentManager.backStackEntryCount > 0 )
                supportFragmentManager.popBackStack()
        }
    }
}