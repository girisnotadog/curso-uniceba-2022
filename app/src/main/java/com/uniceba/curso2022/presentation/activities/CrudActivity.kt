package com.uniceba.curso2022.presentation.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.uniceba.curso2022.R
import com.uniceba.curso2022.databinding.ActivityCrudBinding
import com.uniceba.curso2022.presentation.fragments.CreateItemFragment
import com.uniceba.curso2022.presentation.fragments.ListCrudFragment
import com.uniceba.curso2022.presentation.fragments.SharedPreferencesFragment

class CrudActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCrudBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCrudBinding.inflate( layoutInflater )

        setContentView( binding.root )

        inicializar()
    }

    fun inicializar() {
        binding.activityCrudToolbar.setOnMenuItemClickListener {
            // Identifcamos en que elemento del menú se hizo clic
            when(it.itemId) {
                R.id.activity_crud_toolbar_menu_item_add -> {
                    val fragmentoNuevoRegistro = CreateItemFragment.newInstance()
                    establecerFragmento( fragmentoNuevoRegistro )
                }
                R.id.activity_crud_toolbar_menu_item_sharedpreferences -> {
                    val fragmentoSharedPreferences = SharedPreferencesFragment.newInstance()
                    establecerFragmento( fragmentoSharedPreferences )
                }
            }

            true
        }

        binding.activityCrudToolbar.setNavigationOnClickListener {
            retroceder()
        }

        val fragmentoLista = ListCrudFragment.newInstance()
        establecerFragmento( fragmentoLista )
    }

    fun retroceder() {
        // Si la cantidad de fragmentos en la pila es mayor a 1
        if( supportFragmentManager.backStackEntryCount > 1 )
        // Sacamos el fragmento de la pila
            supportFragmentManager.popBackStack()
    }

    fun editar( id: String ) {
        val fragmentoEditar = CreateItemFragment.newInstance( id )
        establecerFragmento( fragmentoEditar )
    }

    private fun establecerFragmento( fragmento: Fragment ) {
        supportFragmentManager.beginTransaction()
            .replace(
                binding.activityCrudFragmentContainer.id,
                fragmento
            )
            .addToBackStack(null)
            .commit()
    }
}