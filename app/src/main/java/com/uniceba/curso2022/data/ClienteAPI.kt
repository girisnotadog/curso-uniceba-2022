package com.uniceba.curso2022.data

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ClienteAPI {
    const val URL_BASE = "https://6337770d132b46ee0be1be67.mockapi.io/uniceba/api/"

    val retrofit = Retrofit.Builder()
        .baseUrl(URL_BASE)
        .addConverterFactory(
            GsonConverterFactory.create()
        )
        .build()

    val miServicio = retrofit.create( ServiciosAPI::class.java )
}