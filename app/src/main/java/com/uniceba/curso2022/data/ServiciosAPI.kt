package com.uniceba.curso2022.data

import com.uniceba.curso2022.data.dto.AlumnosDto
import com.uniceba.curso2022.data.dto.PublicApisEntriesDto
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

// Todas las url que vamos a consumir
interface ServiciosAPI {
    // Nombre de la url que vamos a consumir
    @GET("alumnos")
    // Función que puede suspender o detener temporalmente la ejecución del código
    suspend fun obtenerEntradas(): List<AlumnosDto>

    // Obtenemos UN SOLO REGISTRO de tipo AlumnosDto
    @GET("alumnos/{id}")
    suspend fun obtenerRegistro(
        @Path("id") id: String
    ): AlumnosDto

    @POST("alumnos")
    suspend fun guardarEntrada(
        @Body alumnosDto: AlumnosDto
    ): AlumnosDto

    @PUT("alumnos/{id}")
    suspend fun actualizarEntrada(
        @Path("id") id: String,
        @Body alumnosDto: AlumnosDto
    ): AlumnosDto

    @DELETE("alumnos/{id}")
    suspend fun eliminarRegistro(
        @Path("id") id: String
    )
}