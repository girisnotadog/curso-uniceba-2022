package com.uniceba.curso2022.data.dto

// Define la respuesta del servicio
// DTO = Data Transfer Object
data class PublicApisEntriesDto(
    val count: Int,
    val entries: List<PublicApiEntry>
) {
    data class PublicApiEntry(
        val API:            String, // "AdoptAPet",
        val Description:    String, // "Resource to help get pets adopted",
        val Auth:           String, // "apiKey",
        val HTTPS:          Boolean, // true,
        val Cors:           String, // "yes",
        val Link:           String, // "https://www.adoptapet.com/public/apis/pet_list.html",
        val Category:       String, // "Animals"
    )
}
