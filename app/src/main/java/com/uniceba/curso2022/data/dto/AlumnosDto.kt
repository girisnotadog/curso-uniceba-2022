package com.uniceba.curso2022.data.dto

data class AlumnosDto(
    val pet: String = "",
    val name: String = "", //" "Roman Kuhn",
    val avatar: String = "", // "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/153.jpg",
    val age: String = "", // "2022-03-22T01:30:37.471Z",
    val id: String = "", // "1"
)
